import 'dart:convert';

import 'package:covid19cases/Bloc/app_bloc/app_event.dart';
import 'package:covid19cases/Bloc/app_bloc/app_state.dart';
import 'package:covid19cases/model/case_info.dart';
import 'package:covid19cases/model/chart_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

class AppBloc extends Bloc<AppEvent, AppState> {
  @override
  AppState get initialState => Loading();

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    
    final totalCases = await http.get('https://2019ncov.asia/api/country_region');
    final dataCases = jsonDecode(totalCases.body) as Map<String,Object>;
    final cases = (dataCases['results']as List).map((entry) => CasesInfo.fromJson(entry)).toList();

    final chartCases = await http.get('https://opendata.ecdc.europa.eu/covid19/casedistribution/json/');
    final dataforchart = jsonDecode(chartCases.body) as Map<String,Object>;
    final ct = (dataforchart['records']as List).map((chart) => ChartModel.fromJson(chart)).toList();

     yield Loaded(cases,ct);

    int sum=0;
    print(sum);
    //yield Loaded(allCases);
  }
}
