import 'package:covid19cases/model/case_info.dart';
import 'package:covid19cases/model/chart_model.dart';

abstract class AppState {}

class Loading extends AppState {}

class Loaded extends AppState {
  
  Loaded(this.cases,this.chart);

  final List<CasesInfo> cases;
  final List<ChartModel> chart;

}
