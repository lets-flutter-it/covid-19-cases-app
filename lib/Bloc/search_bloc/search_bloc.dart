
import 'package:covid19cases/Bloc/search_bloc/search_event.dart';
import 'package:covid19cases/Bloc/search_bloc/search_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchBloc extends Bloc<SearchEvent,SearchState>{

  TextEditingController searchcontroll = TextEditingController();

  @override
  SearchState get initialState => Initial();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async*{
    yield Searched(searchcontroll.text.toLowerCase());
  }
  
}