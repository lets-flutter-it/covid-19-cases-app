import 'package:covid19cases/Bloc/app_bloc/app_bloc.dart';
import 'package:covid19cases/Bloc/app_bloc/app_event.dart';
import 'package:covid19cases/Bloc/app_bloc/app_state.dart';
import 'package:covid19cases/screens/home_screen/home_screen.dart';
import 'package:covid19cases/screens/loading_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => AppBloc()..add(FetchData()),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: BlocBuilder<AppBloc, AppState>(
            builder: (_, state) =>
                state is Loaded ? HomeScreen(state.cases,state.chart) : LoadingScreen(),
          ),
        ),
      );
}
