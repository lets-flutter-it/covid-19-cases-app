import 'package:flutter/material.dart';

class CasesInfo{

  const CasesInfo({
    @required this.country,
    @required this.confirmed,
    @required this.deaths,
    @required this.recovered,
    //@required this.datetime,
  });

  factory CasesInfo.fromJson(Map<String,Object> json) => CasesInfo(
    country: json['country_region'],
    confirmed: json['confirmed'],
    deaths: json['deaths'],
    recovered: json['recovered'],
    //datetime : DateTime.utc(json['date']),
  );

  final String country;
  final int confirmed, deaths, recovered ;
  //final DateTime datetime;

  int get active=>confirmed- (deaths+recovered);



}