import 'package:flutter/material.dart';

class ChartModel{

  const ChartModel({
    @required this.confirmed,
    @required this.month,
  });

  factory ChartModel.fromJson(Map<String,Object> json) => ChartModel(
    confirmed: json['cases'],
    month: int.parse(json['month']),
  );


  final int confirmed;
  final int month;

}