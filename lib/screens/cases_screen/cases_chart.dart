import 'package:covid19cases/model/chart_model.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

double sumcases(List<ChartModel> cm,int mo)
{
  double sum = 0;
  cm.forEach((element) { 
    if(element.month == mo)
      sum+=element.confirmed;
  }
  );
  print(sum);
  return sum;
}

class CasesChart extends StatelessWidget {
  CasesChart(this.caseschart);
  final List<ChartModel> caseschart;

  final List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  @override
  Widget build(BuildContext context) => AspectRatio(
        aspectRatio: 16 / 9,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(30)),
          padding: EdgeInsets.all(15),
          child: LineChart(
            LineChartData(
              borderData: FlBorderData(
                border: Border.symmetric(
                  vertical: BorderSide(color: Colors.black),
                ),
              ),
              gridData: FlGridData(
                checkToShowHorizontalLine: (d) => d % 100000 == 0,
              ),
              minX: 1,
              minY: 0,
              maxY: 5000000,
              lineBarsData: [
                LineChartBarData(
                  isStepLineChart: false,
                  curveSmoothness: 0.3,
                  spots: [
                    FlSpot(6, sumcases(caseschart, 6)),
                    FlSpot(5, sumcases(caseschart, 5)),
                    FlSpot(4, sumcases(caseschart, 4)),
                    FlSpot(3, sumcases(caseschart, 3)),
                    FlSpot(2, sumcases(caseschart, 2)),
                    FlSpot(1, sumcases(caseschart, 1)),
                  ],
                  colors: gradientColors,
                  barWidth: 6.0,
                  isCurved: true,
                  isStrokeCapRound: true,
                  preventCurveOverShooting: true,
                  belowBarData: BarAreaData(
                    show: true,
                    colors: gradientColors
                        .map((color) => color.withOpacity(0.3))
                        .toList(),
                  ),
                ),
              ],
              titlesData: FlTitlesData(
                  show: true,
                  leftTitles: SideTitles(
                    showTitles: true,
                    interval: 1000000,
                    margin: 10,
                    textStyle: TextStyle(color: Colors.black),
                    reservedSize: 25,
                  ),
                  bottomTitles: SideTitles(
                    showTitles: true,
                    getTitles: (d) => [
                      'Jan',
                      'Feb',
                      'Mar',
                      'Apr',
                      'May',
                      'Jun',
                      'Jul',
                      'Aug',
                      'Sep',
                      'Oct',
                      'Nov',
                      'Dec',
                    ][d.toInt() - 1],
                    textStyle: TextStyle(color: Colors.black),
                  )),
            ),
          ),
        ),
      );
}
