import 'package:covid19cases/Bloc/search_bloc/search_bloc.dart';
import 'package:covid19cases/Bloc/search_bloc/search_event.dart';
import 'package:covid19cases/Bloc/search_bloc/search_state.dart';
import 'package:covid19cases/model/case_info.dart';
import 'package:covid19cases/screens/about_screen/about_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../logo.dart';
import 'case_card.dart';

class CasesList extends StatelessWidget {
  const CasesList(this.cases);

  final List<CasesInfo> cases;

  @override
  Widget build(BuildContext context) => BlocProvider(
    create: (_)=> SearchBloc(),
    child: Scaffold(
      body: Stack(
          children: [
            Image.asset(
              'assets/covid-19.jpg',
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.blue.withOpacity(.3),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: BlocBuilder<SearchBloc,SearchState>(
                  builder: (context, state) {
                    return Column(
                      children: [
                        Row( //The changes startes from this line to ->
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Logo(),
                            IconButton(
                              icon: Icon(Icons.question_answer),
                              onPressed: () => Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (_) => About(), // Here should be the ABOUT page call
                                  )
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(controller: BlocProvider.of<SearchBloc>(context).searchcontroll,
                            onChanged:(e)=> BlocProvider.of<SearchBloc>(context).add(Search()),
                            decoration: InputDecoration(
                              hintText: "Search For Country.....",
                              border: OutlineInputBorder(),
                            ),
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        if(state is Initial)
                          Expanded(
                            child: ListView(
                              children: [
                                ...cases.map((c) => CaseCard(c)),
                              ],
                            ),
                          ),
                        if(state is Searched)
                          Expanded(
                            child: ListView(
                              children: [
                                ...cases.where((element) =>element.country.toLowerCase().contains(state.text))
                                .map((c) => CaseCard(c)),
                              ],
                            ),
                          ),
                      ],
                    );
                  }
                ),
              ),
            ),
          ]
      ),
    ),
  );
}
