import 'package:covid19cases/model/case_info.dart';
import 'package:covid19cases/model/chart_model.dart';
import 'package:covid19cases/screens/cases_screen/cases_chart.dart';
import 'package:flutter/material.dart';

import 'cases_list.dart';

int total(List<CasesInfo> caseinfo) {
  int sum = 0;
  caseinfo.forEach((element) {
    sum += element.confirmed;
  });
  return sum;
}

int death(List<CasesInfo> caseinfo) {
  int sum = 0;
  caseinfo.forEach((element) {
    sum += element.deaths;
  });
  return sum;
}

int recovered(List<CasesInfo> caseinfo) {
  int sum = 0;
  caseinfo.forEach((element) {
    sum += element.recovered;
  });
  return sum;
}

int active(List<CasesInfo> caseinfo) {
  int sum = 0;
  caseinfo.forEach((element) {
    sum += element.active;
  });
  return sum;
}

class TotalCases extends StatelessWidget {
  TotalCases(this.cases,this.caseschart);

  final List<CasesInfo> cases;
  final List<ChartModel> caseschart;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => CasesList(cases)));
        },
        child: Card(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(55),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Text(
                    'Total Cases',
                    style: TextStyle(fontSize: 23),
                  ),
                  Text(
                    total(cases).toString(),
                    style: TextStyle(fontSize: 23, color: Colors.red),
                  ),
                  Divider(),
                  Expanded(
                    child: CasesChart(caseschart),
                  ),
                  Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Column(
                        children: <Widget>[
                          Text(
                            'Active',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            active(cases).toString(),
                            style: TextStyle(fontSize: 23, color: Colors.red),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            'Deaths',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            death(cases).toString(),
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.blue.shade900,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            'Recovered',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            recovered(cases).toString(),
                            style: TextStyle(
                              fontSize: 23,
                              color: Colors.green.shade500,
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            )),
      );
}
