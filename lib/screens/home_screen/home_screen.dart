import 'package:covid19cases/model/case_info.dart';
import 'package:covid19cases/model/chart_model.dart';
import 'package:covid19cases/model/resources_safety.dart';
import 'package:covid19cases/screens/about_screen/about_screen.dart';
import 'package:covid19cases/screens/cases_screen/total_cases.dart';
import 'package:covid19cases/screens/resources_for_safety_list/resources_safety_list.dart';
import 'package:flutter/material.dart';

import '../logo.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen(this.cases,this.caseschart);

  final List<CasesInfo> cases;
  final List<ChartModel> caseschart;

  final resources = [
    ResourcesSafety(
        text: 'waer the mask',
        img: 'mask.png',
        link: 'https://www.google.com/'),
    ResourcesSafety(
        text: 'stay at home',
        img: 'stay-at-home.png',
        link: 'https://www.facebok.com/'),
    ResourcesSafety(
        text: 'wash your hands',
        img: 'wash-your-hands.png',
        link: 'https://www.youtube.com/'),
    ResourcesSafety(
        text: 'safety distance',
        img: 'social.png',
        link: 'https://www.vk.com/'),
  ];

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Logo(),
          actions: [
            IconButton(
              icon: Icon(Icons.info, color: Colors.black),
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => About(), // Here should be the ABOUT page call
                ),
              ),
            ),
          ],
          elevation: 25,
        ),
        body: Stack(children: [
          Image.asset(
            'assets/covid-19.jpg',
            height: double.infinity,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Colors.blue.shade200.withOpacity(.4),
              Colors.blue.withOpacity(.5),
              Colors.blue.shade800.withOpacity(.6),
            ])),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: [
                  SizedBox(height: 20),
                  ResourcesSafetyList(resources),
                  Expanded(child: TotalCases(cases,caseschart)),
                ],
              ),
            ),
          ),
        ]),
      );
}
