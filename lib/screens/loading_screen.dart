import 'package:flutter/material.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen();
  @override
  Widget build(BuildContext context) => SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/logo.png'),
              CircularProgressIndicator(),
          ],
        ),
      ),
    ),
  );
}
